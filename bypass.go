// Public Access Client. Copyright (c) 2021 Kona Arctic. All rights reserved. NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com
// Connect to Public Access server
package paclient
import "errors"
import "io"
import tcpmux "gitlab.com/public-internet/tcpomux"
import "os"

type Edge struct{
	Country string
	Method string
	Cookie string
}

type Bypass struct{
	Edges [ ]Edge
	Token string
	Country string
	dialer tcpmux.Dialer
	await [ ]chan error
	stream io.ReadWriteCloser
}

// Connect to a server given enough information
func ( self * Edge )Connect( ) ( io.ReadWriteCloser , error ) {
	var err error
	var stream io.ReadWriteCloser
	if methods[ self.Method ] == nil {
		return nil , errors.New( "method " + self.Method + " not defined" ) }
	stream , err = methods[ self.Method ]( self.Cookie )
	if err != nil {
		return nil , err }
	return stream , nil
}

// Connect to server by country code
func ( self * Bypass )Connect( ) ( tcpmux.Dialer , error ) {
	var err , err1 error

	// Concurrency
	self.await = append( self.await , make( chan error ) )
	if len( self.await ) > 1 {
		return self.dialer , <- self.await[ len( self.await ) - 1 ] }
	defer func( ){
		for _ , value := range self.await[ 1 : ] {
			value <- err }
		self.await = [ ]chan error{ }
	}( )

	// Try each server TODO: try all servers simultaneously and return fastest 
	for _ , edge := range self.Edges {
		if  self.Country == "" ||
		    edge.Country == self.Country {

			self.stream , err = edge.Connect( )
			if err != nil {
				_ , err1 = os.Stderr.Write( [ ]byte( "W Method " + edge.Method + " to " + edge.Country + " failed " + err.Error( ) + "\r\n" ) )
				if err1 != nil {
					return self.dialer , err1 }
				return self.dialer , err
			} else {
				_ , err1 = os.Stderr.Write( [ ]byte( "N Method " + edge.Method + " to " + edge.Country + " works!\r\n" ) )
				if err1 != nil {
					return self.dialer , err1 }

				// Apply TCPoMux
				var config tcpmux.Config = tcpmux.Config{
					Dialer : fakedial ,
					LowLat : true ,
				}
				err = config.Start( self.stream )
				if err != nil {
					return self.dialer , err }
				self.dialer = config.Dialer

				_ , err1 = os.Stderr.Write( [ ]byte( "N Connection established!\r\n" ) )
				if err1 != nil {
					return self.dialer , err1 }
				return self.dialer , nil
			}
		}
	}

	// :(
	if err == nil {
		err = errors.New( "unknown country" + self.Country )
	} else {
		err = errors.New( "nothing works :(" ) }
	return self.dialer , err
}

func ( self * Bypass )Terminate( )error {
	if len( self.await ) > 0 {
		self.await = append( self.await , make( chan error ) )
		<- self.await[ len( self.await ) - 1 ]
	}
	self.dialer = nil
	if self.stream != nil {
		return self.stream.Close( )
	} else {
		return nil
	}
}

