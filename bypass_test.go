package paclient
import "testing"

func TestBypass( test * testing.T ) {
	var err error
	var bypass Bypass

	bypass.Edges = [ ]Edge{
		Edge{
			Method : "not a method" ,
		} ,
	}
	_ , err = bypass.Connect( )
	if err == nil {
		test.Fatalf( "%v\r\n" , "didnt return error" ) }
	if err.Error( ) != "method not a method not defined" {
		test.Errorf( "%v\r\n" , "incorrect error " + err.Error( ) )
	}

	bypass.Edges = [ ]Edge{
		Edge{
			Method : "direct" ,
			Cookie : "public-internet-europe.herokuapp.com" ,
		} ,
	}
	dialer , err := bypass.Connect( )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

	// FIXME Test doesnt work without this and I cant figure out
	socket , err := dialer( "tcp" , "example.com:80" )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }
	_ , err = socket.Write( [ ]byte( "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n" ) )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

	for _ , _ = range [ ]uint8{ 0 , 0 , 0 , 0 } {
		_ , err := bypass.Connect( )
		if err != nil {
			test.Fatalf( "%v\r\n" , err ) }
	}
	err = bypass.Terminate( )
	if err != nil {
		test.Fatalf( "%v\r\n" , err ) }

}


