// Public Access Client. Copyright (c) 2021 Kona Arctic. All rights reserved. NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com
// HTTP CONNECT handle
package paclient
import "io"
import "net"
import "net/http"
import "os"

// Handle
func ( self Bypass )ServeHTTP( response http.ResponseWriter , request * http.Request ) {
	var err error
	var hijack http.Hijacker
	var socket , stream net.Conn

	// Check request
	if request.Method != "CONNECT" {
		request.URL.Scheme = "https"
		response.Header( )[ "Location" ] = [ ]string{ request.URL.String( ) }
		response.WriteHeader( http.StatusTemporaryRedirect )
		return
	}

	// Make socket
	_ , err = os.Stderr.Write( [ ]byte( "D Request " + request.Host + "\r\n" ) )
	if err != nil {
		response.WriteHeader( http.StatusInternalServerError )
		return }
	if self.dialer == nil {
		_ , err = os.Stderr.Write( [ ]byte( "E self.dialer is nil \r\n" ) )
		response.WriteHeader( http.StatusInternalServerError )
		return }
	socket , err = self.dialer( "tcp" , request.Host )
	if err != nil {	// FIXME

		// Reconnect
		_ , err = os.Stderr.Write( [ ]byte( "E Connection died " + err.Error( ) + "\r\n" ) )
		if err != nil {
			response.WriteHeader( http.StatusInternalServerError )
			return }
		_ , err = self.Connect( )
		if err != nil {
			_ , err = os.Stderr.Write( [ ]byte( "E Cant reconnect " + err.Error( ) + "\r\n" ) )
			if err != nil {
				response.WriteHeader( http.StatusInternalServerError )
				return }
			response.WriteHeader( http.StatusBadGateway )
			return }
		self.ServeHTTP( response , request )
		return

	}

	// Hijack
	hijack , ok := response.( http.Hijacker )
	if ! ok {
		response.WriteHeader( http.StatusInternalServerError )
		_ , _ = response.Write( [ ]byte( "response does not implement http.Hijacker" ) )
		return }
	stream , _ , err = hijack.Hijack( )
	if err != nil {
		response.WriteHeader( http.StatusInternalServerError )
		_ , _ = response.Write( [ ]byte( err.Error( ) ) )
		return }
	_ , err = stream.Write( [ ]byte( "HTTP/1.1 200 OK\r\n\r\n" ) )
	if err != nil {
		return }

	// Copy
	go io.Copy( socket , stream )
	go io.Copy( stream , socket )

	return
}

