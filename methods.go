// Public Access Client. Copyright (c) 2021 Kona Arctic. All rights reserved. NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com
// List of bypass methods
package paclient
import httpbase "gitlab.com/public-internet/http-base"
//import shadow "gitea.kona.cf/PublicAccess/Shadow"
import "io"

var methods map[ string ]func( string )( io.ReadWriteCloser , error ) = map[ string ]func( string )( io.ReadWriteCloser , error ){
	"direct" : httpbase.Connect ,
//	"shadow" : shadow.Shadow ,
}

