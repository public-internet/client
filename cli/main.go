// Public Access Client. Copyright (c) 2021 Kona Arctic. All rights reserved. NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com
// HTTP CONNECT proxy server
package main
import "encoding/json"
import "flag"
import paclient "gitlab.com/public-internet/Client"
import "net/http"
import "os"
import "strings"

func main( ) {
	var err error
	var bypassString string
	var server http.Server
	var bypass paclient.Bypass
	_ , err = os.Stderr.Write( [ ]byte( "L Copyright (c) 2021 Kona Arctic. All rights reserved. ABSOLUTELY NO WARRANTY! https://akona.me mailto:arcticjieer@gmail.com\r\n" ) )
	if err != nil {
		os.Exit( 10 ) }

	// Parse arguments
	flag.Usage = func( ) {
		_ , err = flag.CommandLine.Output( ).Write( [ ]byte( "For help and usage please contact software provider.\r\n" ) ) 
		if err != nil {
			_ , _ = os.Stderr.Write( [ ]byte( "F " + err.Error( ) ) )
			os.Exit( 10 ) }
	}
	flag.StringVar( & server.Addr , "listen" , ternary( os.Getenv( "PORT" ) == "" , "8080" , os.Getenv( "PORT" ) ).( string ) , "" )
	flag.StringVar( & bypass.Country , "country" , "" , "" )
	flag.StringVar( & bypass.Token , "token" , "6BQAAQAAAAAAAAAAAAIAAAAAtMH9-g4AAAAAAAsUhPPchHvRPxOSKoY63vEIbw3Vlwxy9hUPkrFudWxs" , "" )
	flag.StringVar( & bypassString , "server" , "[{\"country\":\"ie\",\"method\":\"direct\",\"cookie\":\"public-internet-europe.herokuapp.com\"}]" , "" )
	flag.Parse( )

	// Parse server list
	servefile , err := os.Open( bypassString )
	if err == nil {
		decoder := json.NewDecoder( servefile )
		err = decoder.Decode( & bypass.Edges )
	} else if strings.Contains( err.Error( ) , "no such file or directory" ) ||
		  strings.Contains( err.Error( ) , "file name too long" ) {
		decoder := json.NewDecoder( strings.NewReader( bypassString ) )
		err = decoder.Decode( & bypass.Edges )
	}
	if err != nil {
		os.Stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
		os.Exit( 30 )
	}

	// Connect to Public Access servers
//	if country != "" {
		_ , err = bypass.Connect( )
//	} else {
//		_ , err = bypass.Best( )
//	}
	if err != nil {
		_ , _ = os.Stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
		os.Exit( 40 )
	}
	_ , err = os.Stderr.Write( [ ]byte( "N Connection established\r\n" ) )
	if err != nil {
		os.Exit( 10 ) }

	// Serve
	server.Handler = bypass
	server.Addr = "[::]:" + server.Addr
	_ , err = os.Stderr.Write( [ ]byte( "N Listening " + server.Addr + "\r\n" ) )
	if err != nil {
		os.Exit( 10 ) }
	err = server.ListenAndServe( )
	_ , err = os.Stderr.Write( [ ]byte( "F " + err.Error( ) + "\r\n" ) )
	os.Exit( 10 )

	return
}


